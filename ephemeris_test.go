package ephemeris

import (
	"gitlab.com/ydisanto/go-ephemeris/angle"

	"testing"
	"time"
)

func TestCalculateSunriseAndSunset(t *testing.T) {
	// Arrange
	datetime, err := time.Parse(time.RFC3339, "2021-11-27T01:10:05+01:00")
	if err != nil {
		t.Error(err)
	}
	latitude := angle.Degrees(43.813910083662975)
	longitude := angle.Degrees(1.3286816116351632)

	// Act
	sunrise, sunset := CalculateSunriseAndSunset(datetime, latitude, longitude)

	// Assert
	assert := assertWith{t}
	assert.thatDuration(sunrise).equals(time.Duration(484) * time.Minute)
	assert.thatDuration(sunset).equals(time.Duration(1040) * time.Minute)
}

type assertWith struct {
	t *testing.T
}

type assertDuration struct {
	t        *testing.T
	duration time.Duration
}

func (assert assertWith) thatDuration(duration time.Duration) assertDuration {
	return assertDuration{
		t:        assert.t,
		duration: duration,
	}
}

func (asserted assertDuration) equals(expected time.Duration) {
	if asserted.duration.Nanoseconds() != expected.Nanoseconds() {
		asserted.t.Error("duration", asserted.duration, "is not equal to asserted duration", expected)
	}
}
