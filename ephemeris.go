package ephemeris

import (
	"gitlab.com/ydisanto/go-ephemeris/angle"
	"gitlab.com/ydisanto/go-ephemeris/julian"

	"math"
	"time"
)

func CalculateSunGeometricMeanLongitude(T julian.JulianCenturies) angle.Angle {
	tValue := T.Value()
	L0 := 280.46646 + tValue*(36000.76983+tValue*(0.0003032))
	for L0 > 360.0 {
		L0 -= 360.0
	}
	for L0 < 0.0 {
		L0 += 360.0
	}
	return angle.Degrees(L0)
}

func CalculateSunMeanAnomaly(T julian.JulianCenturies) angle.Angle {
	tValue := T.Value()
	M := 357.52911 + tValue*(35999.05029-0.0001537*tValue)
	return angle.Degrees(M)
}

func CalculateEarthOrbitEccentricity(T julian.JulianCenturies) float64 {
	tValue := T.Value()
	e := 0.016708634 - tValue*(0.000042037+0.0000001267*tValue)
	return e
}

func CalculateSunCenterEquation(T julian.JulianCenturies) angle.Angle {
	tValue := T.Value()
	M := CalculateSunMeanAnomaly(T).Radians()
	sinM := math.Sin(M)
	sin2M := math.Sin(2 * M)
	sin3M := math.Sin(3 * M)
	C := sinM*(1.914602-tValue*(0.004817+0.000014*tValue)) + sin2M*(0.019993-0.000101*tValue) + sin3M*0.000289
	return angle.Degrees(C)
}

func CalculateSunTrueLongitude(T julian.JulianCenturies) angle.Angle {
	L0 := CalculateSunGeometricMeanLongitude(T)
	C := CalculateSunCenterEquation(T)
	return L0.Plus(C)
}

func CalculateSunApparentLongitude(T julian.JulianCenturies) angle.Angle {
	sunTrueLongitude := CalculateSunTrueLongitude(T).Degrees()
	omega := angle.Degrees(125.04 - 1934.136*T.Value())
	lambda := sunTrueLongitude - 0.00569 - 0.00478*angle.Sin(omega)
	return angle.Degrees(lambda)
}

func CalculateMeanObliquityOfEcliptic(T julian.JulianCenturies) angle.Angle {
	tValue := T.Value()
	seconds := 21.448 - tValue*(46.8150+tValue*(0.00059-tValue*(0.001813)))
	e0 := 23.0 + (26.0+(seconds/60.0))/60.0
	return angle.Degrees(e0)
}

func CalculateObliquityCorrection(T julian.JulianCenturies) angle.Angle {
	e0 := CalculateMeanObliquityOfEcliptic(T).Degrees()
	omega := angle.Degrees(125.04 - 1934.136*T.Value())
	e := e0 + 0.00256*angle.Cos(omega)
	return angle.Degrees(e)
}

func CalculateSunDeclination(T julian.JulianCenturies) angle.Angle {
	e := CalculateObliquityCorrection(T)
	lambda := CalculateSunApparentLongitude(T)
	sint := angle.Sin(e) * angle.Sin(lambda)
	theta := angle.Radians(math.Asin(sint))
	return theta
}

func CalculateEquationOfTime(T julian.JulianCenturies) time.Duration {
	epsilon := CalculateObliquityCorrection(T).Radians()
	l0 := CalculateSunGeometricMeanLongitude(T).Radians()
	e := CalculateEarthOrbitEccentricity(T)
	m := CalculateSunMeanAnomaly(T).Radians()

	y := math.Tan(epsilon / 2.0)
	y *= y

	sin2l0 := math.Sin(2.0 * l0)
	sinm := math.Sin(m)
	cos2l0 := math.Cos(2.0 * l0)
	sin4l0 := math.Sin(4.0 * l0)
	sin2m := math.Sin(2.0 * m)

	EtimeRad := y*sin2l0 - 2.0*e*sinm + 4.0*e*y*sinm*cos2l0 - 0.5*y*y*sin4l0 - 1.25*e*e*sin2m
	minutes := angle.Radians(EtimeRad).Degrees() * 4.0
	return time.Duration(minutes) * time.Minute // in minutes of time
}

func CalculateAngleForSunriseHour(latitude angle.Angle, sunDeclination angle.Angle) angle.Angle {
	HAarg := angle.Cos(angle.Degrees(90.833))/(angle.Cos(latitude)*angle.Cos(sunDeclination)) - angle.Tan(latitude)*angle.Tan(sunDeclination)
	HA := math.Acos(HAarg)
	return angle.Radians(HA) // in radians (for sunset, use -HA)
}

func CalculateSunriseAndSunset(datetime time.Time, latitude angle.Angle, longitude angle.Angle) (time.Duration, time.Duration) {
	utcSunrise, utcSunset := CalculateSunriseAndSunsetUTC(datetime, latitude, longitude)
	_, zoneOffsetSeconds := datetime.Zone()
	offset := time.Duration(zoneOffsetSeconds) * time.Second
	return utcSunrise + offset, utcSunset + offset
}

func CalculateSunriseAndSunsetUTC(datetime time.Time, latitude angle.Angle, longitude angle.Angle) (time.Duration, time.Duration) {
	T := julian.Centuries(datetime)
	eqTime := CalculateEquationOfTime(T)
	sunDeclination := CalculateSunDeclination(T)
	hourAngle := CalculateAngleForSunriseHour(latitude, sunDeclination)

	sunriseMinutes := 720 - (4.0 * (longitude.Degrees() + hourAngle.Degrees())) - eqTime.Minutes()
	sunsetMinutes := 720 - (4.0 * (longitude.Degrees() + -hourAngle.Degrees())) - eqTime.Minutes()

	sunrise := time.Duration(sunriseMinutes) * time.Minute
	sunset := time.Duration(sunsetMinutes) * time.Minute
	return sunrise, sunset
}
