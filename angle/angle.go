package angle

import "math"

type Angle float64

func Radians(rad float64) Angle {
	return Angle(rad)
}

func Degrees(degrees float64) Angle {
	return Angle(math.Pi * degrees / 180.0)
}

func (angle Angle) Radians() float64 {
	return float64(angle)
}

func (angle Angle) Degrees() float64 {
	return 180.0 * angle.Radians() / math.Pi
}

func (angle Angle) Times(n float64) Angle {
	return Angle(n * angle.Radians())
}

func (angle Angle) Plus(other Angle) Angle {
	return Angle(angle.Radians() + other.Radians())
}

func Cos(angle Angle) float64 {
	return math.Cos(angle.Radians())
}

func Sin(angle Angle) float64 {
	return math.Sin(angle.Radians())
}

func Tan(angle Angle) float64 {
	return math.Tan(angle.Radians())
}
