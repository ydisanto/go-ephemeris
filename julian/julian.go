package julian

import (
	"math"
	"time"
)

type (
	JulianDay       float64
	JulianCenturies float64
)

func (jd JulianDay) Value() float64 {
	return float64(jd)
}

func (jc JulianCenturies) Value() float64 {
	return float64(jc)
}

type localDate struct {
	Year       int
	Month      time.Month
	DayOfMonth int
}

func dateFrom(datetime time.Time) localDate {
	return localDate{
		Year:       datetime.Year(),
		Month:      datetime.Month(),
		DayOfMonth: datetime.Day(),
	}
}

func (date localDate) isNotJulian() bool {
	return !date.isJulian()
}

func (date localDate) isJulian() bool {
	if date.Year > 1582 {
		return false
	}
	if date.Year < 1582 {
		return true
	}
	// year is 1582 so check month
	if date.Month > time.October {
		return false
	}
	if date.Month < time.October {
		return true
	}
	// month is october so check day of month
	return date.DayOfMonth <= 14
}

func calculateJulianDay(datetime time.Time) JulianDay {
	if datetime.Month() <= time.February {
		datetime = datetime.AddDate(-1, 12, 0)
	}

	var B float64 = 0
	year := float64(datetime.Year())
	if dateFrom(datetime).isNotJulian() {
		A := math.Floor(year / 100.0)
		B = 2 - A + math.Floor(A/4)
	}
	month := float64(datetime.Month())
	dayOfMonth := float64(datetime.Day())
	julianDay :=
		math.Floor(365.25*(year+4716)) +
			math.Floor(30.6001*(month+1)) + dayOfMonth + B - 1524.5
	return JulianDay(julianDay)
}

func Centuries(datetime time.Time) JulianCenturies {
	julianDay := calculateJulianDay(datetime)
	const julianDayReference = 2451545.0
	const referenceTime = 36525.0
	return JulianCenturies((julianDay.Value() - julianDayReference) / referenceTime)
}
